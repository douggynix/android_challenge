/*Author : James Douglass Lefruit
 * Date created : March 22nd
 */

package com.feed.spotifyfeed;


import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.os.Build;

import it.sephiroth.android.library.widget.AbsHListView;
import it.sephiroth.android.library.widget.AdapterView;
import it.sephiroth.android.library.widget.AdapterView.OnItemClickListener;
import it.sephiroth.android.library.widget.HListView;
import it.sephiroth.android.library.widget.AbsHListView.OnScrollListener;

public class AlbumViewerActivity extends Activity implements  OnScrollListener, AlbumDownloaderListener, OnItemClickListener{

	private AlbumFeedAdapter adapter= null;
	private boolean isLoading=false;
	private ProgressBar progressBar=null;
	private TextView loadingTxt=null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.album_viewer_layout);
		progressBar=(ProgressBar) this.findViewById(R.id.progressBar);
		loadingTxt=(TextView)this.findViewById(R.id.loadingTxt);
		HListView albumScroller= (HListView)this.findViewById(R.id.hListView);
	
		adapter= new AlbumFeedAdapter(this);
		albumScroller.setAdapter(adapter);
		albumScroller.setOnScrollListener(this);
		albumScroller.setOnItemClickListener(this);
		
		
		AlbumDataSource albumSource=new AlbumDataSource();
		
		AlbumDownloaderTask dwnTask=new AlbumDownloaderTask(this, "http://charts.spotify.com/api/charts/most_streamed/us/","latest");
		dwnTask.addAlbumDownloaderListener(this);
		dwnTask.execute();
	}

	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.album_viewer, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onScrollStateChanged(AbsHListView view, int scrollState) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onScroll(AbsHListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		//Log.v("Scroll ","1st visible item = "+firstVisibleItem+" ; visible Item Count = "+visibleItemCount);
		if(totalItemCount==0){
			return;
		}
		
		if(this.isLoading){
			return;
		}
		
		if ((firstVisibleItem + visibleItemCount) == totalItemCount){
			
			AlbumFeedAdapter albumAdapter = (AlbumFeedAdapter) view.getAdapter();
			AlbumDataSource albumSource=albumAdapter.getAlbumDataSource();
			AlbumModel album = albumSource.getAlbum(firstVisibleItem);
			if(album.getPrevDate()!=null){
				Log.v("AlbumViewer", "Loading Previous Albums : "+album.getPrevDate());
				AlbumDownloaderTask dwnTask=new AlbumDownloaderTask(this, "http://charts.spotify.com/api/charts/most_streamed/us/",album.getPrevDate());
				dwnTask.addAlbumDownloaderListener(this);
				dwnTask.execute();
				this.isLoading=true;
			}
		}
		
	}


	@Override
	public void onDownloadExecute() {
		// TODO Auto-generated method stub
		this.isLoading=true;
		progressBar.setVisibility(View.VISIBLE);
		loadingTxt.setText("Loading more albums...");
		loadingTxt.setVisibility(View.VISIBLE);
		
	}


	@Override
	public void onAlbumDownloading(int current, int total, AlbumModel album) {
		// TODO Auto-generated method stub
		progressBar.setMax(total);
		progressBar.setProgress(current+1);
		loadingTxt.setText("Loading more albums...");
	}


	@Override
	public void onAlbumDownloadFinished(AlbumDataSource albSource) {
		// TODO Auto-generated method stub
		this.adapter.addAlbumDataSource(albSource);
		this.isLoading = false;
		
		progressBar.setVisibility(View.INVISIBLE);
		progressBar.setProgress(0);
		loadingTxt.setVisibility(View.INVISIBLE);
		//progressBar.
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Intent intent=new Intent(this,TrackPreviewerActivity.class);
		AlbumModel album=(AlbumModel)this.adapter.getItem(position);
		intent.putExtra("album", album);
		this.startActivity(intent);
		//intent.getExtras().getSer
	}

}
