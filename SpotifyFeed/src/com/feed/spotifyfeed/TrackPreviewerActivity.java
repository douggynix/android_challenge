/*Author : James Douglass Lefruit
 * * Date created : March 22nd
 */

package com.feed.spotifyfeed;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;

public class TrackPreviewerActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.track_viewer_layout);
		Bundle extras=this.getIntent().getExtras();
		AlbumModel album = (AlbumModel)extras.getSerializable("album");
		Log.v("TrackPreviewer", "Album Name preview : "+album.getName());
		
		ImageView albumImgView= (ImageView)this.findViewById(R.id.albumArtView);
		Bitmap bitmap=BitmapFactory.decodeFile(album.getAlbumArtPath());
		albumImgView.setImageBitmap(bitmap);
		String headers[]={	"Artist", 
							"Song", 
							"Album", 
							"Stream Counts",
							"Date"
						};
		String values[]={	album.getArtist(),
							album.getMainTrack(),
							album.getName(),
							""+album.getStreamCount(),
							album.getDate()
						};
		
		ListView albumInfoView=(ListView) this.findViewById(R.id.albumINfoList);
		albumInfoView.setAdapter(new TrackAdapter(this, headers, values));
	}

}
