/*Author : James Douglass Lefruit
 * Date created : March 22nd
 */

package com.feed.spotifyfeed;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class AlbumModel implements Serializable{
	private String name;
	private String mainTrack;
	private String date;
	private String prevDate;
	private String nextDate;
	private String artist;
	private String country;
	private String albumCategory;
	private String albumArtUrl;
	private String albumArtPath;
	private String mostStreamedDate;
	private int streamCount;
	
	private ArrayList<String> songs =  new ArrayList<String>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAlbumArtUrl() {
		return albumArtUrl;
	}
	public void setAlbumArtUrl(String albumArtUrl) {
		this.albumArtUrl = albumArtUrl;
	}
	public String getAlbumArtPath() {
		return albumArtPath;
	}
	public void setAlbumArtPath(String albumArtPath) {
		this.albumArtPath = albumArtPath;
	}
	public String getMainTrack() {
		return mainTrack;
	}
	public void setMainTrack(String mainTrack) {
		this.mainTrack = mainTrack;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPrevDate() {
		return prevDate;
	}
	public void setPrevDate(String prevDate) {
		this.prevDate = prevDate;
	}
	public String getNextDate() {
		return nextDate;
	}
	public void setNextDate(String nextDate) {
		this.nextDate = nextDate;
	}
	public void addSongs(String song){
		this.songs.add(song);
	}
	
	public int getStreamCount() {
		return streamCount;
	}
	public void setStreamCount(int streamCount) {
		this.streamCount = streamCount;
	}
	public String toString(){
		return this.getArtist()+" - "+ this.getMainTrack()+" ( "+this.getName()+" ) ";
	}
	public String getAlbumCategory() {
		return albumCategory;
	}
	public void setAlbumCategory(String albumCategory) {
		this.albumCategory = albumCategory;
	}
	
}
