/*Author : James Douglass Lefruit
 * Date created : March 22nd
 */

package com.feed.spotifyfeed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TrackAdapter extends BaseAdapter {
	private Context context;
	private String headers[];
	private String values[];
	public TrackAdapter(Context c, String headers[], String values[]){
		this.context = c;
		this.headers =headers;
		this.values = values;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return this.headers.length;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(view == null){
			view=LayoutInflater.from(this.context).inflate(R.layout.track_item_layout, parent,false);
		}
			
		TextView headerTxt = (TextView)view.findViewById(R.id.headerTxt);
		TextView valueTxt = (TextView)view.findViewById(R.id.valueTxt);
			
		headerTxt.setText(this.headers[position]);
		valueTxt.setText(this.values[position]);
		
		return view;
	}



}
