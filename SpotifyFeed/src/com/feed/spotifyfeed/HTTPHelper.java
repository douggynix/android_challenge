/*Author : James Douglass Lefruit
 * Date created : March 22nd
 */

package com.feed.spotifyfeed;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

public class HTTPHelper {
	private Context context;
	public HTTPHelper(Context c){
		this.context = c;
	}

	
	public String getHttpContent(String baseUrl) throws Exception{
		URL url = new URL(baseUrl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setReadTimeout(5000);
		conn.setConnectTimeout(5000);
		conn.setRequestMethod("GET");
		conn.connect();
		
		int code = conn.getResponseCode();
		String response=null;
		StringBuffer strBuffer=new StringBuffer();
		if(code == HttpURLConnection.HTTP_OK)
		{
			InputStream input = conn.getInputStream();
			BufferedReader bufInput= new BufferedReader(new InputStreamReader(input));
			byte buffer[]=new byte[8192];
			int total_bytes=-1;
			while((total_bytes=input.read(buffer)) !=-1){
				strBuffer.append(new String(buffer,0,total_bytes));
			}
			response=strBuffer.toString();
			
		}
		else {
			throw new IOException("HTTP Request Error "+code);
		}
		return response;
	}
	
	
	public JSONObject convertStringToJSon(String json) throws JSONException{
		JSONObject jsonObj=new JSONObject(json);
		
		return jsonObj;
		
	}
	

	
	public File downloadPicture(String url){
		InputStream in=null;
		Bitmap bitmap = null;
		File tmpFile=null;
		try {
			 in= new URL(url).openStream();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		try {
			
			Log.v("Download", "Tmp Path : "+ System.getProperty("java.io.tmpdir"));
			Log.v("Download", "Cache Dir : "+ this.context.getCacheDir());
			tmpFile=File.createTempFile("album",".jpg",this.context.getCacheDir());
			
			Log.v("Download", "Picture Path : "+tmpFile.getAbsolutePath());
			byte buffer[]=new byte[8192];
			InputStream input=new BufferedInputStream(in, 8192);
			
			
			FileOutputStream output=new FileOutputStream(tmpFile);
			int total_chars=-1;
			while( (total_chars=input.read(buffer) )!=-1){
				output.write(buffer,0,total_chars);
			}
			output.close();
			
		} 
		catch(Exception e){
			Log.v("Download", "Error on downloading jpg file  : "+e.getMessage());
			e.printStackTrace();
		}
		finally{
			try {
				if(in!=null){
					in.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{
				;
			}
			return tmpFile;
		}
	}

	
	
}
