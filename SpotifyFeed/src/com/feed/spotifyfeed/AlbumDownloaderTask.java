/*Author : James Douglass Lefruit
 * Date created : March 22nd
 */

package com.feed.spotifyfeed;



import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.BaseAdapter;

public class AlbumDownloaderTask extends AsyncTask<Void, Integer, Void>{
	
	private Context context;
	private String url;
	private AlbumDataSource  albumSource=new AlbumDataSource();;
	private AlbumFeedAdapter adapter;
	private String albumCategory;
	
	//Array of listeners for album download
	ArrayList<AlbumDownloaderListener> listeners=new ArrayList<AlbumDownloaderListener>();
	
	public AlbumDownloaderTask(Context c,String baseUrl,String year){
		this.context = c;
		this.url = baseUrl+year;
		this.albumCategory = year;
		
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub
		//super.onProgressUpdate(values);
		//notify listeners about album download progress
		int current=values[0];
		int total_album=values[1];
		AlbumModel album=this.albumSource.getAlbum(current);
		for(AlbumDownloaderListener listener: listeners){
			listener.onAlbumDownloading(current, total_album, album);
		}
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		//notify listeners about download execute
		for(AlbumDownloaderListener listener: listeners){
			listener.onDownloadExecute();
		}
	}
	
	
	
	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		HTTPHelper httpHelper= new HTTPHelper(this.context);
		try {
			String json=httpHelper.getHttpContent(url);
			Log.v("HttpHelper", json);
			this.processJSON(json);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	public void processJSON(String json) throws JSONException{
		
		JSONObject jsonRoot=new JSONObject(json);
		
		String nextDate=null;
		String prevDate = null;
		if (jsonRoot.has("prevDate")){
			prevDate=jsonRoot.getString("prevDate");
		}
		
		if(jsonRoot.has("nextDate")){
			nextDate=jsonRoot.getString("nextDate");
		}
		
		JSONArray jsonArray=jsonRoot.getJSONArray("tracks");
		HTTPHelper httpHelper=new HTTPHelper(this.context);
		//Loop into the tracks to print album names
		 
		for(int i=0;i<jsonArray.length();i++){
			JSONObject oneTrack= jsonArray.getJSONObject(i);
			//String album = oneTrack.getString("album_name");
			AlbumModel album= new AlbumModel();
			album.setMainTrack(oneTrack.getString("track_name"));
			album.setName(oneTrack.getString("album_name"));
			album.setArtist(oneTrack.getString("artist_name"));
			album.setAlbumArtUrl(oneTrack.getString("artwork_url"));
			album.setDate(oneTrack.getString("date"));
			album.setPrevDate(prevDate);
			album.setNextDate(nextDate);
			album.setAlbumCategory(this.albumCategory);
			album.setStreamCount(oneTrack.getInt("num_streams"));
			File albumPic=httpHelper.downloadPicture(album.getAlbumArtUrl());
			album.setAlbumArtPath(albumPic.getAbsolutePath());
			Log.v("HTTPHelper", album.toString());
			albumSource.addAlbum(album);
			Integer[] progress={i,jsonArray.length()};
			//this will call the registered listening for album download. useful for 
			this.publishProgress(progress);
		}
		
	}
	
	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		//notify listeners about album download complete
		for(AlbumDownloaderListener listener : listeners){
			listener.onAlbumDownloadFinished(albumSource);
		}
		//this.adapter.setAlbumDataSource(this.albumSource);
	}
	
	public void addAlbumDownloaderListener(AlbumDownloaderListener listener){
		this.listeners.add(listener);
	}

}
