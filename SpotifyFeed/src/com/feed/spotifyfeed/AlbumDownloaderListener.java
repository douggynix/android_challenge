/*Author : James Douglass Lefruit
 * Date created : March 22nd
 */

package com.feed.spotifyfeed;

//Listener to be registered to execute custom codes during download
public interface AlbumDownloaderListener {
	//When we execute Download
	public void onDownloadExecute();
	public void onAlbumDownloading(int current, int total, AlbumModel album);
	public void onAlbumDownloadFinished(AlbumDataSource albSource);
}
