/*Author : James Douglass Lefruit
 * Date created : March 22nd
 */


package com.feed.spotifyfeed;

import java.util.ArrayList;

public class AlbumDataSource {

	private ArrayList<AlbumModel> albums= new ArrayList<AlbumModel>();

	
	public void addAlbum(AlbumModel album){
		this.albums.add(album);
	}
	
	public int getAlbumLength(){
		return this.albums.size();
	}
	
	public AlbumModel getAlbum(int index){
		return this.albums.get(index);
	}
	
	public void clear(){
		this.albums.clear();
	}
	
	public void removeAlbum(int index){
		this.albums.remove(index);
	}
	
	public void removeAlbum(AlbumModel album){
		this.albums.remove(album);
	}
}


