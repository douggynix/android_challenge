/*Author : James Douglass Lefruit
 * Date created : March 22nd
 */

package com.feed.spotifyfeed;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import it.sephiroth.android.library.widget.AbsHListView;
import it.sephiroth.android.library.widget.AbsHListView.OnScrollListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AlbumFeedAdapter extends BaseAdapter  {

	private Context context;
	private AlbumDataSource albumSource=new AlbumDataSource();
	
	private JSONObject streamJson=new JSONObject();
	
	
	public AlbumFeedAdapter(Context c){
		this.context = c;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		int total= this.albumSource.getAlbumLength();
		return total;
	}

	
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return this.albumSource.getAlbum(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	public void addAlbumDataSource(AlbumDataSource source){
		boolean should_build= this.albumSource.getAlbumLength() != 0;
		for(int i=0;i<source.getAlbumLength();i++){
			
			AlbumModel album=source.getAlbum(i);
			this.addAlbum(album);
			
			
			
		}
//		//build metadata for previous most streamed tracks
//		for(int i=0;i<this.albumSource.getAlbumLength();i++){
//			AlbumModel album=albumSource.getAlbum(i);
//			if(!album.getAlbumCategory().equalsIgnoreCase("latest")){
//				int current=this.getCount()-1;
//				this.buildMostStreamedIndexes(album, albumSource, current);
//			}
//
//		}
//		this.notifyDataSetChanged();
	}

	public void addAlbum(AlbumModel album){
		this.albumSource.addAlbum(album);
		this.notifyDataSetChanged();
	}
	
	public void setAlbumDataSource(AlbumDataSource source){
		this.albumSource =source;
		this.notifyDataSetChanged();
	}
	
	public AlbumDataSource getAlbumDataSource(){
		return this.albumSource;
	}
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(view==null){
		view= LayoutInflater.from( context ).inflate(R.layout.album_item_layout, parent, false);	
		}
		
		AlbumModel album=this.albumSource.getAlbum(position);
		
		ImageView albumView=(ImageView) view.findViewById(R.id.albumArtView);
		
		Bitmap bitmap=BitmapFactory.decodeFile(album.getAlbumArtPath());
		albumView.setImageBitmap(bitmap);
		TextView mostStreamTxt=(TextView) view.findViewById(R.id.mostStreamedTXT);
		mostStreamTxt.setText(album.getArtist());
		/*if(this.streamJson.has(""+position)){
			try {
				mostStreamTxt.setText(this.streamJson.getString(""+position));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{}
		}*/
		return view;
	}
	
	

	

	
	
//	private void buildMostStreamedIndexes(AlbumModel album, AlbumDataSource source, int position){
//		//Query this album in this datasource 
//		ArrayList<Integer> albIndexes=this.queryAlbumIndex(album.getName(), album.getArtist(), album.getMainTrack(), source);
//		if(albIndexes.size() !=0 ){
//			HashMap<String, String> streamHash = new HashMap<String, String>();
//			
//			for(int i=0;i<albIndexes.size();i++){
//				AlbumModel anAlbum = source.getAlbum(albIndexes.get(i).intValue());
//				streamHash.put(""+albIndexes.get(i).intValue(),anAlbum.getStreamCount()+"");
//				//Log.v("Indexes", "Index  "+i+"for position #"+position+ " : "+anAlbum.getStreamCount());
//			}
//			
//			//streamHash.put(albIndexes.size()+"",album.getStreamCount()+"");
//			
//			HashMap sortedStreamHash=this.sortByValues(streamHash);
//			Iterator keyIterator=sortedStreamHash.keySet().iterator();
//			//top album index
//			
//			int topIndex =-1; 
//			int i=0;
//			while(keyIterator.hasNext()){
//				topIndex = Integer.parseInt((String)keyIterator.next());
//				//Log.v("Indexes", "Sort Index  "+topIndex+" position #"+position+ " : "+topIndex);
//				
//			}
//			AlbumModel alb=album;
//			try{
//				if(album.getStreamCount()< source.getAlbum(topIndex).getStreamCount()){
//					//this.streamAnalytics.put(""+position, source.getAlbum(topIndex).getDate()+":"+source.getAlbum(topIndex).getStreamCount());
//					alb=source.getAlbum(topIndex);
//				}
//				else{
//					//this.streamAnalytics.put(""+position, album.getDate()+" : "+album.getStreamCount());
//				}
//				
//				//Log.v("BuildMostStreamed", "Most Stream value for position top index : "+topIndex);
//				this.streamJson.put(""+position,  alb.getDate()+" : "+alb.getStreamCount());
//				//Log.v("BuildMostStreamed", "Most Stream value for position #"+position+ " : "+alb.getStreamCount());
//				//Log.v("BuildMostStreamed Hash", "Most Stream value for position #"+position+ " : "+this.streamJson.get(""+position));
//			}
//			catch(Exception e){
//				Log.v("BuildMostStreamed", "Error : "+topIndex);
//				e.printStackTrace();
//			}
//			finally{
//				;
//			}
//			
//		}
//	}
//	
//	//http://thilinasameera.wordpress.com/2011/06/01/sorting-algorithms-sample-codes-on-java-c-and-matlab/#_Bubble_Sort
//	public int[] cardSort(int[] data){
//		  int len = data.length;
//		  int key = 0;
//		  int i = 0;
//		  for(int j = 1;j<len;j++){
//		    key = data[j];
//		    i = j-1;
//		    while(i>=0 && data[i]>key){
//		      data[i+1] = data[i];
//		      i = i-1;
//		      data[i+1]=key;
//		    }
//		  }
//		  return data;
//		}
//
//
//	private ArrayList<Integer> queryAlbumIndex(String name, String artist, String song, AlbumDataSource source){
//		ArrayList<Integer> indexes=new ArrayList<Integer>();
//		for(int i=0;i<source.getAlbumLength();i++){
//			AlbumModel album=source.getAlbum(i);
//			if(album.getAlbumCategory().equalsIgnoreCase("latest")){
//				continue;
//			}
//			if(album.getName().equalsIgnoreCase(name)   && 
//					album.getMainTrack().equalsIgnoreCase(song) && 
//					album.getArtist().equalsIgnoreCase(artist) ){
//				indexes.add(Integer.valueOf(i));
//			}
//		}
//		return indexes;
//	}
//	
//	
//	
//	//Sort a Hashmap, useful for getting most streamed tracks date
//	//Bonus from : http://beginnersbook.com/2013/12/how-to-sort-hashmap-in-java-by-keys-and-values/
//	HashMap sortByValues(HashMap map) { 
//	       List list = new LinkedList(map.entrySet());
//	       // Defined Custom Comparator here
//	       Collections.sort(list, new Comparator() {
//	            public int compare(Object o1, Object o2) {
//	               return ((Comparable) ((Map.Entry) (o1)).getValue())
//	                  .compareTo(((Map.Entry) (o2)).getValue());
//	            }
//	       });
//
//	       // Here I am copying the sorted list in HashMap
//	       // using LinkedHashMap to preserve the insertion order
//	       HashMap sortedHashMap = new LinkedHashMap();
//	       for (Iterator it = list.iterator(); it.hasNext();) {
//	              Map.Entry entry = (Map.Entry) it.next();
//	              sortedHashMap.put(entry.getKey(), entry.getValue());
//	       } 
//	       return sortedHashMap;
//	  }

}
